package com.keinpyisi.krul.tarothorroscope;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.Collections;

public class SubMainMenu extends AppCompatActivity {
private ImageView major , minor;
private Button draw;
private ProgressDialog pd;
    private ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_main_menu);
        major = findViewById(R.id.majorbtn);
        minor = findViewById(R.id.minorbtn);
        draw = findViewById(R.id.draw);
        major.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"Major Arcana",Toast.LENGTH_LONG).show();
                Intent myIntent = new Intent(SubMainMenu.this, MajorArcana.class);
                //Optional parameters
                SubMainMenu.this.startActivity(myIntent);


            }
        });
        minor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(),"Minor Arcana",Toast.LENGTH_LONG).show();
                Intent myIntent = new Intent(SubMainMenu.this, MinorArcana.class);
                //Optional parameters
                SubMainMenu.this.startActivity(myIntent);


            }
        });
        draw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Toast.makeText(getApplicationContext(),"Please Think Of Question Until Complete",Toast.LENGTH_LONG).show();
                pd = ProgressDialog.show(SubMainMenu.this, "", "Please Wait...",
                        true, false);
                pd.show();
                new CountDownTimer(600, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        //this will be done every 1000 milliseconds ( 1 seconds )
                        Toast.makeText(getApplicationContext(),"Please Think Of Question Until Complete",Toast.LENGTH_LONG).show();
                        long progress = (600 - millisUntilFinished) / 1000;
                        pd.setProgress(Integer.parseInt(progress+""));

                    }

                    @Override
                    public void onFinish() {
                        //the progressBar will be invisible after 60 000 miliseconds ( 1 minute)
                        pd.dismiss();
                        Intent myIntent = new Intent(SubMainMenu.this, DrawCard.class);
                        //Optional parameters
                        SubMainMenu.this.startActivity(myIntent);
                    }

                }.start();



            }
        });
    }
}
