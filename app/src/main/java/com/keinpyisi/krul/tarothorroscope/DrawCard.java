package com.keinpyisi.krul.tarothorroscope;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class DrawCard extends AppCompatActivity {

    private ImageView image;
    private TextView presshere;


    private int[] card= {
            R.drawable.fool,R.drawable.magician,R.drawable.priestress,R.drawable.empress,
            R.drawable.emperor,R.drawable.hierophant,R.drawable.lovers,R.drawable.chariot,
            R.drawable.justice,R.drawable.hermit,R.drawable.fortune,R.drawable.strength,R.drawable.hangedmen,
            R.drawable.death,R.drawable.temperance,R.drawable.tower,R.drawable.devil,R.drawable.star,R.drawable.moon,R.drawable.sun
            ,R.drawable.judgement,R.drawable.world
    };
    private int[] cardreversed = {
            R.drawable.foolreversed,R.drawable.magicianreversed,R.drawable.priestressreversed,R.drawable.empreversed,
            R.drawable.emperorreversed,R.drawable.hieroreversed,R.drawable.loreversed,R.drawable.chareversed,
            R.drawable.justreversed,R.drawable.herreversed,R.drawable.forreversed,R.drawable.strengthreversed,R.drawable.hangreversed,
            R.drawable.deadreversed,R.drawable.temperancereversed,R.drawable.toreversed,R.drawable.devilreversed,R.drawable.streversed,R.drawable.moreversed,R.drawable.sureversed
            ,R.drawable.jureversed,R.drawable.worldreversed
    };
    private int[] wands = {
            R.drawable.oneofwands,R.drawable.twoofwands,R.drawable.threeofwands,R.drawable.fourofwand,R.drawable.fiveofwand,
            R.drawable.sixofwand,R.drawable.sevenofwand,R.drawable.eightofwand,R.drawable.nineofwand,R.drawable.tenofwands,
            R.drawable.pageofwands,R.drawable.knightofwands,R.drawable.queenofwands,R.drawable.kingofwands
    };
    private int[] wandsreverse = {
            R.drawable.oneofwandsreversed,R.drawable.twoofwandsreversed,R.drawable.threeofwandsreversed,R.drawable.fourofwandreversed,R.drawable.fiveofwandreversed,
            R.drawable.sixofwandreversed,R.drawable.sevenofwandreversed,R.drawable.eightofwandreversed,R.drawable.nineofwandreversed,R.drawable.tenofwandsreversed,
            R.drawable.pagwanre,R.drawable.knightofwandsreversed,R.drawable.queenofwandsreversed,R.drawable.kingofwandsreversed
    };
    private int[] cups = {
            R.drawable.oneofcup,R.drawable.twoofcup,R.drawable.threeofcup,R.drawable.fourofcup,R.drawable.fiveofcup,R.drawable.sixofcup,
            R.drawable.sevenofcup,R.drawable.eightofcup,R.drawable.nineofcup,R.drawable.tenofcup,R.drawable.pageofcup,R.drawable.knightofcup,
            R.drawable.queenofcup,R.drawable.kingofcup
    };
    private int[] cupsreverse = {
            R.drawable.oneofcupreversed,R.drawable.twoofcupreversed,R.drawable.threeofcupreversed,R.drawable.fourofcupreversed,R.drawable.fiveofcupreversed,R.drawable.sixofcupreversed,
            R.drawable.sevenofcupreversed,R.drawable.eightofcupreversed,R.drawable.nineofcupreversed,R.drawable.tenofcupreversed,R.drawable.cuppagere,R.drawable.knightofcupreversed,
            R.drawable.queenofcupreversed,R.drawable.kingofcupreversed
    };
    private int[] pentacles = {
            R.drawable.oneofpentacle,R.drawable.twoofpentacle,R.drawable.threeofpentacle,R.drawable.fourofpentacle,R.drawable.fiveofpentacle,
            R.drawable.sixofpentacle,R.drawable.sevenofpentacle,R.drawable.eightofpentacle,R.drawable.nineofpentacle,R.drawable.tenofpentacle,
            R.drawable.pageofpentacle,R.drawable.knightofpentacle,R.drawable.queenofpentacle,R.drawable.kingofpentacle
    };
    private int[] pentaclesreverse = {
            R.drawable.oneofpentaclereversed,R.drawable.twoofpentaclereversed,R.drawable.threeofpentaclereversed,R.drawable.fourofpentaclereversed,R.drawable.fiveofpentaclereversed,
            R.drawable.sixofpentaclereversed,R.drawable.sevenofpentaclereversed,R.drawable.eightofpentaclereversed,R.drawable.nineofpentaclereversed,R.drawable.tenofpentaclereversed,
            R.drawable.pageofpentaclereversed,R.drawable.knightofpentaclereversed,R.drawable.quepenre,R.drawable.kingofpentaclereversed
    };
    private int[] swords = {
            R.drawable.oneofswords,R.drawable.twoofswords,R.drawable.threeofswords,R.drawable.fourofswords,R.drawable.fiveofswords,
            R.drawable.sixofswords,R.drawable.sevenofswords,R.drawable.eightofswords,R.drawable.nineofswords,R.drawable.tenofswords,
            R.drawable.pageofswords,R.drawable.knightofswords,R.drawable.queenofswords,R.drawable.kingofswords
    };
    private int[] swordsreverse = {
            R.drawable.oneofswordsreversed,R.drawable.twoofswordsreversed,R.drawable.threeofswordsreversed,R.drawable.fourofswordsreversed,R.drawable.fiveofswordsreversed,
            R.drawable.sixofswordsreversed,R.drawable.sevenofswordsreversed,R.drawable.eightofswordsreversed,R.drawable.nineofswordsreversed,R.drawable.tenofswordsreversed,
            R.drawable.pageofswordsreversed,R.drawable.knightofswordsreversed,R.drawable.queswre,R.drawable.kingofswordsreversed
    };
    ArrayList<Integer>all = new ArrayList<>();
    ArrayList<Integer>first = new ArrayList<>();
    ArrayList<Integer>second = new ArrayList<>();
    int result;
    int index;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw_card);
        image = findViewById(R.id.drawcard);
    presshere = findViewById(R.id.clickhere);

        for(int a = 0 ; a < card.length ; a++){
            all.add(card[a]);
            all.add(cardreversed[a]);
        }

        for(int c = 0 ; c<wands.length;c++){
            all.add(wands[c]);
            all.add(wandsreverse[c]);
            all.add(cups[c]);
            all.add(cupsreverse[c]);
            all.add(pentacles[c]);
            all.add(pentaclesreverse[c]);
            all.add(swords[c]);
            all.add(swordsreverse[c]);
        }
            // do stuff
            Collections.shuffle(all);

        for(int j=0 ; j<all.size()/2 ; j++){
            first.add(all.get(j));
        }
        for(int k = all.size()/2;k<all.size(); k++){
            second.add(all.get(k));
        }
all.clear();
for(int z=0 ;z<second.size();z++){
      all.add(second.get(z));
}

        for(int y=0 ;y<second.size();y++){
            all.add(first.get(y));
        }
        result = all.get(0);

        image.setImageResource(result);
        presshere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String name = getResources().getResourceName(result);

                datasearch(name);

            }
        });


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        all.clear();
        first.clear();
        second.clear();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        all.clear();
        first.clear();
        second.clear();
    }
    public void datasearch(String name){
        Log.d("TAG",name);
        if(name.toLowerCase().contains("cup") &&name.toLowerCase().contains("reversed")){
            if(name.toLowerCase().contains("one")){
                String nametwo = "The Ace Cups Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("two")){
                String nametwo = "The 2 Of Cups Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("three")){
                String nametwo = "The 3 Of Cups Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("four")){
                String nametwo = "The 4 Of Cups Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("five")){
                String nametwo = "The 5 Of Cups Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("six")){
                String nametwo = "The 6 Of Cups Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("seven")){
                String nametwo = "The 7 Of Cups Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("eight")){
                String nametwo = "The 8 Of Cups Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("nine")){
                String nametwo = "The 9 Of Cups Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("ten")){
                String nametwo = "The 10 Of Cups Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("page")){
                String nametwo = "The Page Of Cups Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("knight")){
                String nametwo = "The Knight Of Cups Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("queen")){
                String nametwo = "The Queen Of Cups Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("king")){
                String nametwo = "The King Of Cups Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }
        }else  if(name.toLowerCase().contains("wands") &&name.toLowerCase().contains("reversed")){
            if(name.toLowerCase().contains("one")){
                String nametwo = "The Ace Of Wands Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("two")){
                String nametwo = "The 2 Of Wands Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("three")){
                String nametwo = "The 3 Of Wands Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("four")){
                String nametwo = "The 4 Of Wands Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("five")){
                String nametwo = "The 5 Of Wands Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("six")){
                String nametwo = "The 6 Of Wands Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("seven")){
                String nametwo = "The 7 Of Wands Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("eight")){
                String nametwo = "The 8 Of Wands Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("nine")){
                String nametwo = "The 9 Of Wands Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("ten")){
                String nametwo = "The 10 Of Wands Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("page")){
                String nametwo = "The Page Of Wands Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("knight")){
                String nametwo = "The Knight Of Wands Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("queen")){
                String nametwo = "The Queen Of Wands Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("king")){
                String nametwo = "The King Of Wands Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }
        }else  if(name.toLowerCase().contains("swords") &&name.toLowerCase().contains("reversed")){
            if(name.toLowerCase().contains("one")){
                String nametwo = "The Ace Of Swords Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("two")){
                String nametwo = "The 2 Of Swords Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("three")){
                String nametwo = "The 3 Of Swords Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("four")){
                String nametwo = "The 4 Of Swords Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("five")){
                String nametwo = "The 5 Of Swords Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("six")){
                String nametwo = "The 6 Of Swords Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("seven")){
                String nametwo = "The 7 Of Swords Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("eight")){
                String nametwo = "The 8 Of Swords Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("nine")){
                String nametwo = "The 9 Of Swords Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("ten")){
                String nametwo = "The 10 Of Swords Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("page")){
                String nametwo = "The Page Of Swords Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("knight")){
                String nametwo = "The Knight Of Swords Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("que")){
                String nametwo = "The Queen Of Swords Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("king")){
                String nametwo = "The King Of Swords Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }
        } else if(name.toLowerCase().contains("pen") &&name.toLowerCase().contains("re")){
            if(name.toLowerCase().contains("one")){
                String nametwo = "The Ace Of Pentacles Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("two")){
                String nametwo = "The 2 Of Pentacles Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("three")){

                String nametwo = "The 3 Of Pentacles Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("four")){

                String nametwo = "The 4 Of Pentacles Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("five")){

                String nametwo = "The 5 Of Pentacles Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("six")){

                String nametwo = "The 6 Of Pentacles Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("seven")){

                String nametwo = "The 7 Of Pentacles Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("eight")){

                String nametwo = "The 8 Of Pentacles Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);

                finish();
            }else if(name.toLowerCase().contains("nine")){

                String nametwo = "The 9 Of Pentacles Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("ten")){

                String nametwo = "The 10 Of Pentacles Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("page")){

                String nametwo = "The Page Of Pentacles Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("knight")){

                String nametwo = "The Knight Of Pentacles Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("que")){

                String nametwo = "The Queen Of Pentacles Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("king")){

                String nametwo = "The King Of Pentacles Reversed";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }
        }else if(name.toLowerCase().contains("pentacles") ){
            if(name.toLowerCase().contains("one")){

                String nametwo = "The Ace Of Pentacles";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("two")){

                String nametwo = "The 2 Of Pentacles";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("three")){

                String nametwo = "The 3 Of Pentacles";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("four")){

                String nametwo = "The 4 Of Pentacles";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("five")){

                String nametwo = "The 5 Of Pentacles";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("six")){

                String nametwo = "The 6 Of Pentacles";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("seven")){

                String nametwo = "The 7 Of Pentacles";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("eight")){

                String nametwo = "The 8 Of Pentacles";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("nine")){

                String nametwo = "The 9 Of Pentacles";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("ten")){

                String nametwo = "The 10 Of Pentacles";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("page")){

                String nametwo = "The Page Of Pentacles";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("knight")){

                String nametwo = "The Knight Of Pentacles";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("queen")){

                String nametwo = "The Queen Of Pentacles";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("king")){

                String nametwo = "The King Of Pentacles";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();
            }
        }else if(name.toLowerCase().contains("swords") ){
            if(name.toLowerCase().contains("one")){

                String nametwo = "The Ace Of Swords";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("two")){

                String nametwo = "The 2 Of Swords";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("three")){

                String nametwo = "The 3 Of Swords";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("four")){

                String nametwo = "The 4 Of Swords";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("five")){

                String nametwo = "The 5 Of Swords";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("six")){

                String nametwo = "The 6 Of Swords";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("seven")){

                String nametwo = "The 7 Of Swords";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("eight")){

                String nametwo = "The 8 Of Swords";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("nine")){

                String nametwo = "The 9 Of Swords";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("ten")){

                String nametwo = "The 10 Of Swords";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("page")){

                String nametwo = "The Page Of Swords";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("knight")){

                String nametwo = "The Knight Of Swords";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("queen")){

                String nametwo = "The Queen Of Swords";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("king")){

                String nametwo = "The King Of Swords";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }
        }else if(name.toLowerCase().contains("wan") ){
            if(name.toLowerCase().contains("one")){

                String nametwo = "The Ace Of Wands";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("two")){

                String nametwo = "The 2 Of Wands";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("three")){

                String nametwo = "The 3 Of Wands";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("four")){

                String nametwo = "The 4 Of Wands";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("five")){

                String nametwo = "The 5 Of Wands";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("six")){

                String nametwo = "The 6 Of Wands";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("seven")){

                String nametwo = "The 7 Of Wands";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("eight")){

                String nametwo = "The 8 Of Wands";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();
            }else if(name.toLowerCase().contains("nine")){

                String nametwo = "The 9 Of Wands";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("ten")){

                String nametwo = "The 10 Of Wands";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("pag")){

                String nametwo = "The Page Of Wands";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("knight")){

                String nametwo = "The Knight Of Wands";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("queen")){

                String nametwo = "The Queen Of Wands";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("king")){

                String nametwo = "The King Of Wands";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }
        }else if(name.toLowerCase().contains("cup") ){
            if(name.toLowerCase().contains("one")){

                String nametwo = "The Ace Cups";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("two")){

                String nametwo = "The 2 Of Cups";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("three")){

                String nametwo = "The 3 Of Cups";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("four")){

                String nametwo = "The 4 Of Cups";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("five")){

                String nametwo = "The 5 Of Cups";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("six")){

                String nametwo = "The 6 Of Cups";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("seven")){

                String nametwo = "The 7 Of Cups";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("eight")){

                String nametwo = "The 8 Of Cups";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("nine")){

                String nametwo = "The 9 Of Cups";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("ten")){

                String nametwo = "The 10 Of Cups";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("page")){

                String nametwo = "The Page Of Cups";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("knight")){

                String nametwo = "The Knight Of Cups";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("queen")){

                String nametwo = "The Queen Of Cups";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }else if(name.toLowerCase().contains("king")){

                String nametwo = "The King Of Cups";
                Intent intent = new Intent(getBaseContext(), CardDetail.class);
                intent.putExtra("EXTRA_SESSION_ID", nametwo);
                intent.putExtra("FROM","MINOR");
                startActivity(intent);
                finish();

            }
        }else if(name.toLowerCase().contains("fool")&&name.toLowerCase().contains("reversed")){
            String nametwo = "The Fool Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("magician")&&name.toLowerCase().contains("reversed")){
            String nametwo = "The Magician Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("priestress")&&name.toLowerCase().contains("reversed")){
            String nametwo = "The High Priestess Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("emp")&&name.toLowerCase().contains("reversed")){
            String nametwo = "The Empress Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("emperor")&&name.toLowerCase().contains("reversed")){
            String nametwo = "The Emperor Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("hiero")&&name.toLowerCase().contains("reversed")){
            String nametwo = "The Hierophant Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("lo")&&name.toLowerCase().contains("reversed")){
            String nametwo = "The Lovers Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("cha")&&name.toLowerCase().contains("reversed")){
            String nametwo = "The Chariot Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("justice")&&name.toLowerCase().contains("reversed")){
            String nametwo = "Justice Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("her")&&name.toLowerCase().contains("reversed")){
            String nametwo = "Hermit Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("for")&&name.toLowerCase().contains("reversed")){
            String nametwo = "The Wheel Of Fortune Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("strength")&&name.toLowerCase().contains("reversed")){
            String nametwo = "Strength Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }
        else if(name.toLowerCase().contains("hang")&&name.toLowerCase().contains("reversed")){
            String nametwo = "The Hanged Man Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }
        else if(name.toLowerCase().contains("dead")&&name.toLowerCase().contains("reversed")){
            String nametwo = "Death Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("temperance")&&name.toLowerCase().contains("reversed")){
            String nametwo = "Temperance Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("devil")&&name.toLowerCase().contains("reversed")){
            String nametwo = "The Devil Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("to")&&name.toLowerCase().contains("reversed")){
            String nametwo = "The Tower Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("st")&&name.toLowerCase().contains("reversed")){
            String nametwo = "The Star Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("mo")&&name.toLowerCase().contains("reversed")){
            String nametwo = "The Moon Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("sun")&&name.toLowerCase().contains("reversed")){
            String nametwo = "The Sun Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("ju")&&name.toLowerCase().contains("reversed")){
            String nametwo = "Judgement Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("world")&&name.toLowerCase().contains("reversed")){
            String nametwo = "The World Reversed";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("fool")){
            String nametwo = "The Fool";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("magician")){
            String nametwo = "The Magician";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("priestress")){
            String nametwo = "The High Priestess";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("empress")){
            String nametwo = "The Empress";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("emperor")){
            String nametwo = "The Emperor";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("hierophant")){
            String nametwo = "The Hierophant";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("lovers")){
            String nametwo = "The Lovers";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("chariot")){
            String nametwo = "The Chariot";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("justice")){
            String nametwo = "Justice";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("hermit")){
            String nametwo = "Hermit";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("fortune")){
            String nametwo = "The Wheel Of Fortune";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("strength")){
            String nametwo = "Strength";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }
        else if(name.toLowerCase().contains("hanged")){
            String nametwo = "The Hanged Man";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }
        else if(name.toLowerCase().contains("death")){
            String nametwo = "Death";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("temperance")){
            String nametwo = "Temperance";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("devil")){
            String nametwo = "The Devil";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("tower")){
            String nametwo = "The Tower";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("star")){
            String nametwo = "The Star";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("moon")){
            String nametwo = "The Moon";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("sun")){
            String nametwo = "The Sun";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("judgement")){
            String nametwo = "Judgement";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }else if(name.toLowerCase().contains("world")){
            String nametwo = "The World";
            Intent intent = new Intent(getBaseContext(), CardDetail.class);
            intent.putExtra("EXTRA_SESSION_ID", nametwo);
            intent.putExtra("FROM","MAJOR");
            startActivity(intent);
            finish();
        }

    }
}
