package com.keinpyisi.krul.tarothorroscope;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.util.Arrays;

public class MinorArcana extends AppCompatActivity implements View.OnClickListener {
private ImageView onecup,twocup,threecup,fourcup,fivecup,sixcup,sevencup,eightcup,ninecup,tencup,pagecup,nightcup,queencup,kingcup;
private ImageView onewand,twowand,threewand,fourwand,fivewand,sixwand,sevenwand,eightwand,ninewand,tenwand,pagewand,nightwand,queenwand,kingwand;
private ImageView onepen,twopen,threepen,fourpen,fivepen,sixpen,sevenpen,eightpen,ninepen,tenpen,pagepen,nightpen,queenpen,kingpen;
private ImageView onesword,twosword,threesword,foursword,fivesword,sixsword,sevensword,eightsword,ninesword,tensword,pagesword,nightsword,queensword,kingsword;
    String name="";
    String selectedItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_minor_arcana);
        Initialize();
        action();
    }


    public void action(){
        onecup.setOnClickListener(this);
        twocup.setOnClickListener(this);
        threecup.setOnClickListener(this);
        fourcup.setOnClickListener(this);
        fivecup.setOnClickListener(this);
        sixcup.setOnClickListener(this);
        sevencup.setOnClickListener(this);
        eightcup.setOnClickListener(this);
        ninecup.setOnClickListener(this);
        tencup.setOnClickListener(this);
        pagecup.setOnClickListener(this);
        nightcup.setOnClickListener(this);
        queencup.setOnClickListener(this);
        kingcup.setOnClickListener(this);
        onepen.setOnClickListener(this);
        twopen.setOnClickListener(this);
        threepen.setOnClickListener(this);
        fourpen.setOnClickListener(this);
        fivepen.setOnClickListener(this);
        sixpen.setOnClickListener(this);
        sevenpen.setOnClickListener(this);
        eightpen.setOnClickListener(this);
        ninepen.setOnClickListener(this);
        tenpen.setOnClickListener(this);
        pagepen.setOnClickListener(this);
        kingpen.setOnClickListener(this);
        queenpen.setOnClickListener(this);
        nightpen.setOnClickListener(this);
        onewand.setOnClickListener(this);
        twowand.setOnClickListener(this);
        threewand.setOnClickListener(this);
        fourwand.setOnClickListener(this);
        fivewand.setOnClickListener(this);
        sixwand.setOnClickListener(this);
        sevenwand.setOnClickListener(this);
        eightwand.setOnClickListener(this);
        ninewand.setOnClickListener(this);
        tenwand.setOnClickListener(this);
        kingwand.setOnClickListener(this);
        queenwand.setOnClickListener(this);
        pagewand.setOnClickListener(this);
        nightwand.setOnClickListener(this);
        onesword.setOnClickListener(this);
        twosword.setOnClickListener(this);
        threesword.setOnClickListener(this);
        foursword.setOnClickListener(this);
        fivesword.setOnClickListener(this);
        sixsword.setOnClickListener(this);
        sevensword.setOnClickListener(this);
        eightsword.setOnClickListener(this);
        ninesword.setOnClickListener(this);
        tensword.setOnClickListener(this);
        pagesword.setOnClickListener(this);
        nightsword.setOnClickListener(this);
        kingsword.setOnClickListener(this);
        queensword.setOnClickListener(this);


    }



    public void Initialize(){
        onecup = findViewById(R.id.onecup);
        twocup = findViewById(R.id.twocup);
        threecup = findViewById(R.id.threecup);
        fourcup = findViewById(R.id.fourcup);
        fivecup = findViewById(R.id.fivecup);
        sixcup = findViewById(R.id.sixcup);
        sevencup = findViewById(R.id.sevencup);
        eightcup = findViewById(R.id.eightcup);
        ninecup = findViewById(R.id.ninecup);
        tencup = findViewById(R.id.tencup);
        pagecup = findViewById(R.id.pagecup);
        nightcup = findViewById(R.id.knightcup);
        queencup = findViewById(R.id.queencup);
        kingcup = findViewById(R.id.kingcup);
        onewand = findViewById(R.id.onewand);
        twowand = findViewById(R.id.twowand);
        threewand =  findViewById(R.id.threewand);
        fourwand = findViewById(R.id.fourwand);
        fivewand = findViewById(R.id.fivewand);
        sixwand = findViewById(R.id.sixwand);
        sevenwand = findViewById(R.id.sevenwand);
        eightwand = findViewById(R.id.eightwand);
        ninewand = findViewById(R.id.ninewand);
        tenwand = findViewById(R.id.tenwand);
        pagewand = findViewById(R.id.pagewand);
        nightwand = findViewById(R.id.knightwand);
        queenwand = findViewById(R.id.queenwand);
        kingwand = findViewById(R.id.kingwand);
        onepen = findViewById(R.id.onepen);
        twopen = findViewById(R.id.twopen);
        threepen = findViewById(R.id.threepen);
        fourpen = findViewById(R.id.fourpen);
        fivepen = findViewById(R.id.fivepen);
        sixpen = findViewById(R.id.sixpen);
        sevenpen = findViewById(R.id.sevenpen);
        eightpen = findViewById(R.id.eightpen);
        ninepen = findViewById(R.id.ninepen);
        tenpen = findViewById(R.id.tenpen);
        pagepen = findViewById(R.id.pagepen);
        nightpen = findViewById(R.id.knightpen);
        queenpen = findViewById(R.id.queenpen);
        kingpen = findViewById(R.id.kingpen);
        onesword = findViewById(R.id.onesword);
        twosword = findViewById(R.id.twosword);
        threesword = findViewById(R.id.threesword);
        foursword = findViewById(R.id.foursword);
        fivesword = findViewById(R.id.fivesword);
        sixsword = findViewById(R.id.sixsword);
        sevensword = findViewById(R.id.sevensword);
        eightsword = findViewById(R.id.eightsword);
        ninesword = findViewById(R.id.ninesword);
        tensword = findViewById(R.id.tensword);
        pagesword = findViewById(R.id.pagesword);
        nightsword = findViewById(R.id.knightsword);
        queensword = findViewById(R.id.queensword);
        kingsword = findViewById(R.id.kingsword);
    }

    @Override
    public void onClick(View view) {
        ImageView img =(ImageView)view;
        if(img == onecup){
            name = "The Ace Cups";
            openAlert(name);
        }else if(img == twocup){
name = "The 2 Of Cups";
            openAlert(name);

        }else if(img == threecup){
            name = "The 3 Of Cups";
            openAlert(name);

        }else if(img == fourcup){
            name = "The 4 Of Cups";
            openAlert(name);

        }else if(img == fivecup){
            name = "The 5 Of Cups";
            openAlert(name);

        }else if(img == sixcup){
            name = "The 6 Of Cups";
            openAlert(name);

        }else if(img == sevencup){
            name = "The 7 Of Cups";
            openAlert(name);

        }else if(img == eightcup){
            name = "The 8 Of Cups";
            openAlert(name);

        }else if(img == ninecup){
            name = "The 9 Of Cups";
            openAlert(name);

        }else if(img == tencup){
            name = "The 10 Of Cups";
            openAlert(name);

        }else if(img == pagecup){
            name = "The Page Of Cups";
            openAlert(name);

        }else if(img == nightcup){
            name = "The Knight Of Cups";
            openAlert(name);

        }else if(img == queencup){
            name = "The Queen Of Cups";
            openAlert(name);

        }else if(img == kingcup){
            name = "The King Of Cups";
            openAlert(name);

        }else if(img == onewand){
            name = "The Ace Of Wands";
            openAlert(name);

        }else if(img == twowand){
            name = "The 2 Of Wands";
            openAlert(name);

        }else if(img == threewand){
            name = "The 3 Of Wands";
            openAlert(name);

        }else if(img == fourwand){
            name = "The 4 Of Wands";
            openAlert(name);

        }else if(img == fivewand){
            name = "The 5 Of Wands";
            openAlert(name);

        }else if(img == sixwand){
            name = "The 6 Of Wands";
            openAlert(name);

        }else if(img == sevenwand){
            name = "The 7 Of Wands";
            openAlert(name);

        }else if(img == eightwand){
            name = "The 8 Of Wands";
            openAlert(name);

        }else if(img == ninewand){
            name = "The 9 Of Wands";
            openAlert(name);

        }else if(img == tenwand){
            name = "The 10 Of Wands";
            openAlert(name);

        }else if(img == pagewand){
            name = "The Page Of Wands";
            openAlert(name);

        }else if(img == nightwand){
            name = "The Knight Of Wands";
            openAlert(name);

        }else if(img == queenwand){
            name = "The Queen Of Wands";
            openAlert(name);

        }else if(img == kingwand){
            name = "The King Of Wands";
            openAlert(name);

        } else if (img == onepen) {
name ="The Ace Of Pentacles";
            openAlert(name);

        }else if(img == twopen){
            name ="The 2 Of Pentacles";
            openAlert(name);

        }else if(img == threepen){

            name ="The 3 Of Pentacles";
            openAlert(name);
        }else if(img == fourpen){
            name ="The 4 Of Pentacles";
            openAlert(name);

        }else if(img == fivepen){
            name ="The 5 Of Pentacles";
            openAlert(name);

        }else if(img == sixpen){
            name ="The 6 Of Pentacles";
            openAlert(name);

        }else if(img == sevenpen){
            name ="The 7 Of Pentacles";
            openAlert(name);

        }else if(img == eightpen){
            name ="The 8 Of Pentacles";
            openAlert(name);

        }else if(img == ninepen){
            name ="The 9 Of Pentacles";
            openAlert(name);

        }else if(img == tenpen){
            name ="The 10 Of Pentacles";
            openAlert(name);

        }else if(img == pagepen){

            name ="The Page Of Pentacles";
            openAlert(name);
        }else if(img == nightpen){
            name ="The Knight Of Pentacles";
            openAlert(name);
        }else if(img == queenpen){
            name ="The Queen Of Pentacles";
            openAlert(name);

        }else if(img == kingpen){
            name ="The King Of Pentacles";
            openAlert(name);

        }else if(img == onesword){
name ="The Ace Of Swords";
            openAlert(name);
        }else if(img == twosword){
            name ="The 2 Of Swords";
            openAlert(name);


        }else if(img == threesword){
            name ="The 3 Of Swords";
            openAlert(name);

        }else if(img == foursword){
            name ="The 4 Of Swords";
            openAlert(name);

        }else if(img == fivesword){
            name ="The 5 Of Swords";
            openAlert(name);

        }else if(img == sixsword){
            name ="The 6 Of Swords";
            openAlert(name);

        }else if(img == sevensword){
            name ="The 7 Of Swords";
            openAlert(name);

        }else if(img == eightsword){
            name ="The 8 Of Swords";
            openAlert(name);

        }else if(img == ninesword){
            name ="The 9 Of Swords";
            openAlert(name);

        }else if(img == tensword){
            name ="The 10 Of Swords";
            openAlert(name);

        }else if(img == pagesword){
            name ="The Page Of Swords";
            openAlert(name);

        }else if(img == nightsword){
            name ="The Knight Of Swords";
            openAlert(name);

        }else if(img == queensword){
            name ="The Queen Of Swords";
            openAlert(name);

        }else if(img == kingsword){
            name ="The King Of Swords";
            openAlert(name);

        }



    }
    public void openAlert(String name){
        final String Name = name;

        final AlertDialog.Builder builder = new AlertDialog.Builder(MinorArcana.this);

        // Set the alert dialog title
        builder.setTitle("Choose Position");

        // Initializing an array of flowers
        final String[] position = new String[]{
                "Upright",
                "Reversed",
        };

                /*
                    AlertDialog.Builder setSingleChoiceItems (CharSequence[] items,
                                        int checkedItem,
                                        DialogInterface.OnClickListener listener)

                        Set a list of items to be displayed in the dialog as the content, you will
                        be notified of the selected item via the supplied listener. The list will
                        have a check mark displayed to the right of the text for the checked item.
                        Clicking on an item in the list will not dismiss the dialog. Clicking
                        on a button will dismiss the dialog.

                        Parameters
                            items CharSequence: the items to be displayed.
                            checkedItem int: specifies which item is checked. If -1 no items are checked.
                            listener DialogInterface.OnClickListener: notified when an item on
                                        the list is clicked. The dialog will not be dismissed when
                                        an item is clicked. It will only be dismissed if clicked
                                        on a button, if no buttons are supplied it's up to the
                                        user to dismiss the dialog.
                        Returns
                            AlertDialog.Builder This Builder object to allow for
                                                chaining of calls to set methods
                */

        // Set a single choice items list for alert dialog

        builder.setSingleChoiceItems(
                position, // Items list
                -1, // Index of checked item (-1 = no selection)
                new DialogInterface.OnClickListener() // Item click listener
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Get the alert dialog selected item's text
                        selectedItem = Arrays.asList(position).get(i);

                        // Display the selected item's text on snack bar

                    }
                });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Just dismiss the alert dialog after selection
                // Or do something now
                String nametwo;
                if(selectedItem.toLowerCase().contains("reversed")){

                  nametwo = Name +" "+selectedItem;
                    Intent intent = new Intent(getBaseContext(), CardDetail.class);
                    intent.putExtra("EXTRA_SESSION_ID", nametwo);
                    intent.putExtra("FROM","MINOR");
                    startActivity(intent);

                }else{
                    nametwo = Name;

                    Intent intent = new Intent(getBaseContext(), CardDetail.class);
                    intent.putExtra("EXTRA_SESSION_ID", nametwo);
                    intent.putExtra("FROM","MINOR");
                    startActivity(intent);

                }


            }
        });

        // Create the alert dialog
        AlertDialog dialog = builder.create();

        // Finally, display the alert dialog
        dialog.show();
    }
}
