package com.keinpyisi.krul.tarothorroscope;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class CardDetail extends AppCompatActivity {
private ImageView image;
private TextView intro , general,work,love,finances,health,spirit,title;
    DatabaseHelper myDbHelper;
    ArrayList<String> data;
    private int[] card= {
            R.drawable.fool,R.drawable.magician,R.drawable.priestress,R.drawable.empress,
            R.drawable.emperor,R.drawable.hierophant,R.drawable.lovers,R.drawable.chariot,
            R.drawable.justice,R.drawable.hermit,R.drawable.fortune,R.drawable.strength,R.drawable.hangedmen,
            R.drawable.death,R.drawable.temperance,R.drawable.tower,R.drawable.devil,R.drawable.star,R.drawable.moon,R.drawable.sun
            ,R.drawable.judgement,R.drawable.world
    };
    private int[] cardreversed = {
            R.drawable.foolreversed,R.drawable.magicianreversed,R.drawable.priestressreversed,R.drawable.empreversed,
            R.drawable.emperorreversed,R.drawable.hieroreversed,R.drawable.loreversed,R.drawable.chareversed,
            R.drawable.justreversed,R.drawable.herreversed,R.drawable.forreversed,R.drawable.strengthreversed,R.drawable.hangreversed,
            R.drawable.deadreversed,R.drawable.temperancereversed,R.drawable.toreversed,R.drawable.devilreversed,R.drawable.streversed,R.drawable.moreversed,R.drawable.sureversed
            ,R.drawable.jureversed,R.drawable.worldreversed
    };
    private int[] wands = {
            R.drawable.oneofwands,R.drawable.twoofwands,R.drawable.threeofwands,R.drawable.fourofwand,R.drawable.fiveofwand,
            R.drawable.sixofwand,R.drawable.sevenofwand,R.drawable.eightofwand,R.drawable.nineofwand,R.drawable.tenofwands,
            R.drawable.pageofwands,R.drawable.knightofwands,R.drawable.queenofwands,R.drawable.kingofwands
    };
    private int[] wandsreverse = {
            R.drawable.oneofwandsreversed,R.drawable.twoofwandsreversed,R.drawable.threeofwandsreversed,R.drawable.fourofwandreversed,R.drawable.fiveofwandreversed,
            R.drawable.sixofwandreversed,R.drawable.sevenofwandreversed,R.drawable.eightofwandreversed,R.drawable.nineofwandreversed,R.drawable.tenofwandsreversed,
            R.drawable.pagwanre,R.drawable.knightofwandsreversed,R.drawable.queenofwandsreversed,R.drawable.kingofwandsreversed
    };
    private int[] cups = {
      R.drawable.oneofcup,R.drawable.twoofcup,R.drawable.threeofcup,R.drawable.fourofcup,R.drawable.fiveofcup,R.drawable.sixofcup,
      R.drawable.sevenofcup,R.drawable.eightofcup,R.drawable.nineofcup,R.drawable.tenofcup,R.drawable.pageofcup,R.drawable.knightofcup,
      R.drawable.queenofcup,R.drawable.kingofcup
    };
    private int[] cupsreverse = {
            R.drawable.oneofcupreversed,R.drawable.twoofcupreversed,R.drawable.threeofcupreversed,R.drawable.fourofcupreversed,R.drawable.fiveofcupreversed,R.drawable.sixofcupreversed,
            R.drawable.sevenofcupreversed,R.drawable.eightofcupreversed,R.drawable.nineofcupreversed,R.drawable.tenofcupreversed,R.drawable.cuppagere,R.drawable.knightofcupreversed,
            R.drawable.queenofcupreversed,R.drawable.kingofcupreversed
    };
    private int[] pentacles = {
        R.drawable.oneofpentacle,R.drawable.twoofpentacle,R.drawable.threeofpentacle,R.drawable.fourofpentacle,R.drawable.fiveofpentacle,
        R.drawable.sixofpentacle,R.drawable.sevenofpentacle,R.drawable.eightofpentacle,R.drawable.nineofpentacle,R.drawable.tenofpentacle,
        R.drawable.pageofpentacle,R.drawable.knightofpentacle,R.drawable.queenofpentacle,R.drawable.kingofpentacle
    };
    private int[] pentaclesreverse = {
            R.drawable.oneofpentaclereversed,R.drawable.twoofpentaclereversed,R.drawable.threeofpentaclereversed,R.drawable.fourofpentaclereversed,R.drawable.fiveofpentaclereversed,
            R.drawable.sixofpentaclereversed,R.drawable.sevenofpentaclereversed,R.drawable.eightofpentaclereversed,R.drawable.nineofpentaclereversed,R.drawable.tenofpentaclereversed,
            R.drawable.pageofpentaclereversed,R.drawable.knightofpentaclereversed,R.drawable.quepenre,R.drawable.kingofpentaclereversed
    };
    private int[] swords = {
            R.drawable.oneofswords,R.drawable.twoofswords,R.drawable.threeofswords,R.drawable.fourofswords,R.drawable.fiveofswords,
            R.drawable.sixofswords,R.drawable.sevenofswords,R.drawable.eightofswords,R.drawable.nineofswords,R.drawable.tenofswords,
            R.drawable.pageofswords,R.drawable.knightofswords,R.drawable.queenofswords,R.drawable.kingofswords
    };
    private int[] swordsreverse = {
            R.drawable.oneofswordsreversed,R.drawable.twoofswordsreversed,R.drawable.threeofswordsreversed,R.drawable.fourofswordsreversed,R.drawable.fiveofswordsreversed,
            R.drawable.sixofswordsreversed,R.drawable.sevenofswordsreversed,R.drawable.eightofswordsreversed,R.drawable.nineofswordsreversed,R.drawable.tenofswordsreversed,
            R.drawable.pageofswordsreversed,R.drawable.knightofswordsreversed,R.drawable.queswre,R.drawable.kingofswordsreversed
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_detail);
        Initialization();
        myDbHelper = new DatabaseHelper(getApplicationContext());
        String nametwo= getIntent().getStringExtra("EXTRA_SESSION_ID");
        String from = getIntent().getStringExtra("FROM");
        if(from.equals("MAJOR")){
            data=myDbHelper.getAllData(nametwo);
        if(nametwo.toLowerCase().contains("reversed")){

            if(nametwo.toLowerCase().contains("fool")){
                image.setImageResource(cardreversed[0]);
            }else if(nametwo.toLowerCase().contains("magician")){
                image.setImageResource(cardreversed[1]);

            }else if(nametwo.toLowerCase().contains("priestess")){
                image.setImageResource(cardreversed[2]);

            }else if(nametwo.toLowerCase().contains("empress")){
                image.setImageResource(cardreversed[3]);

            }else if(nametwo.toLowerCase().contains("emperor")){
                image.setImageResource(cardreversed[4]);

            }else if(nametwo.toLowerCase().contains("hierophant")){
                image.setImageResource(cardreversed[5]);

            }else if(nametwo.toLowerCase().contains("lovers")){

                image.setImageResource(cardreversed[6]);
            }else if(nametwo.toLowerCase().contains("chariot")){

                image.setImageResource(cardreversed[7]);
            }else if(nametwo.toLowerCase().contains("justice")){
                image.setImageResource(cardreversed[8]);

            }else if(nametwo.toLowerCase().contains("hermit")){

                image.setImageResource(cardreversed[9]);
            }else if(nametwo.toLowerCase().contains("fortune")){

                image.setImageResource(cardreversed[10]);
            }else if(nametwo.toLowerCase().contains("strength")){

                image.setImageResource(cardreversed[11]);
            }else if(nametwo.toLowerCase().contains("hanged")){

                image.setImageResource(cardreversed[12]);
            }else if(nametwo.toLowerCase().contains("death")){

                image.setImageResource(cardreversed[13]);
            }else if(nametwo.toLowerCase().contains("temperance")){

                image.setImageResource(cardreversed[14]);
            }else if(nametwo.toLowerCase().contains("devil")){

                image.setImageResource(cardreversed[16]);
            }else if(nametwo.toLowerCase().contains("tower")){

                image.setImageResource(cardreversed[15]);
            }else if(nametwo.toLowerCase().contains("star")){

                image.setImageResource(cardreversed[17]);
            }else if(nametwo.toLowerCase().contains("moon")){

                image.setImageResource(cardreversed[18]);
            }else if(nametwo.toLowerCase().contains("sun")){

                image.setImageResource(cardreversed[19]);
            }else if(nametwo.toLowerCase().contains("judgment")){

                image.setImageResource(cardreversed[20]);
            }else {
                image.setImageResource(cardreversed[21]);
            }



        }else {

            if (nametwo.toLowerCase().contains("fool")) {
                image.setImageResource(card[0]);

            } else if (nametwo.toLowerCase().contains("magician")) {
                image.setImageResource(card[1]);

            } else if (nametwo.toLowerCase().contains("priestess")) {
                image.setImageResource(card[2]);

            } else if (nametwo.toLowerCase().contains("empress")) {
                image.setImageResource(card[3]);

            } else if (nametwo.toLowerCase().contains("emperor")) {
                image.setImageResource(card[4]);

            } else if (nametwo.toLowerCase().contains("hierophant")) {
                image.setImageResource(card[5]);

            } else if (nametwo.toLowerCase().contains("lovers")) {
                image.setImageResource(card[6]);

            } else if (nametwo.toLowerCase().contains("chariot")) {
                image.setImageResource(card[7]);

            } else if (nametwo.toLowerCase().contains("justice")) {
                image.setImageResource(card[8]);

            } else if (nametwo.toLowerCase().contains("hermit")) {
                image.setImageResource(card[9]);

            } else if (nametwo.toLowerCase().contains("fortune")) {
                image.setImageResource(card[10]);

            } else if (nametwo.toLowerCase().contains("strength")) {
                image.setImageResource(card[11]);

            } else if (nametwo.toLowerCase().contains("hanged")) {
                image.setImageResource(card[12]);

            } else if (nametwo.toLowerCase().contains("death")) {

                image.setImageResource(card[13]);
            } else if (nametwo.toLowerCase().contains("temperance")) {

                image.setImageResource(card[14]);
            } else if (nametwo.toLowerCase().contains("devil")) {

                image.setImageResource(card[16]);
            } else if (nametwo.toLowerCase().contains("tower")) {

                image.setImageResource(card[15]);
            } else if (nametwo.toLowerCase().contains("star")) {

                image.setImageResource(card[17]);
            } else if (nametwo.toLowerCase().contains("moon")) {
                image.setImageResource(card[18]);
            } else if (nametwo.toLowerCase().contains("sun")) {
                image.setImageResource(card[19]);
            } else if (nametwo.toLowerCase().contains("judgment")) {
                image.setImageResource(card[20]);
            } else {
                image.setImageResource(card[21]);
            }
        }

        }else{
            data=myDbHelper.getMinorAllData(nametwo);
            if(nametwo.toLowerCase().contains("reversed")){


                if(nametwo.toLowerCase().contains("ace") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cupsreverse[0]);
                }else  if(nametwo.toLowerCase().contains("2") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cupsreverse[1]);
                }else  if(nametwo.toLowerCase().contains("3") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cupsreverse[2]);
                }else  if(nametwo.toLowerCase().contains("4") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cupsreverse[3]);
                }else  if(nametwo.toLowerCase().contains("5") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cupsreverse[4]);
                }else  if(nametwo.toLowerCase().contains("6") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cupsreverse[5]);
                }else  if(nametwo.toLowerCase().contains("7") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cupsreverse[6]);
                }else  if(nametwo.toLowerCase().contains("8") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cupsreverse[7]);
                }else  if(nametwo.toLowerCase().contains("9") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cupsreverse[8]);
                }else  if(nametwo.toLowerCase().contains("10") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cupsreverse[9]);
                }else  if(nametwo.toLowerCase().contains("page") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cupsreverse[10]);
                }else  if(nametwo.toLowerCase().contains("knight") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cupsreverse[11]);
                }else  if(nametwo.toLowerCase().contains("queen") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cupsreverse[12]);
                }else  if(nametwo.toLowerCase().contains("king") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cupsreverse[13]);
                }else  if(nametwo.toLowerCase().contains("ace") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wandsreverse[0]);
                }else  if(nametwo.toLowerCase().contains("2") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wandsreverse[1]);
                }else  if(nametwo.toLowerCase().contains("3") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wandsreverse[2]);
                }else  if(nametwo.toLowerCase().contains("4") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wandsreverse[3]);
                }else  if(nametwo.toLowerCase().contains("5") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wandsreverse[4]);
                }else  if(nametwo.toLowerCase().contains("6") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wandsreverse[5]);
                }else  if(nametwo.toLowerCase().contains("7") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wandsreverse[6]);
                }else  if(nametwo.toLowerCase().contains("8") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wandsreverse[7]);
                }else  if(nametwo.toLowerCase().contains("9") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wandsreverse[8]);
                }else  if(nametwo.toLowerCase().contains("10") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wandsreverse[9]);
                }else  if(nametwo.toLowerCase().contains("page") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wandsreverse[10]);
                }else  if(nametwo.toLowerCase().contains("knight") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wandsreverse[11]);
                }else  if(nametwo.toLowerCase().contains("queen") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wandsreverse[12]);
                }
                else  if(nametwo.toLowerCase().contains("king") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wandsreverse[13]);
                }
                else  if(nametwo.toLowerCase().contains("ace") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentaclesreverse[0]);
                }
                else  if(nametwo.toLowerCase().contains("2") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentaclesreverse[1]);
                }
                else  if(nametwo.toLowerCase().contains("3") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentaclesreverse[2]);
                }
                else  if(nametwo.toLowerCase().contains("4") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentaclesreverse[3]);
                }
                else  if(nametwo.toLowerCase().contains("5") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentaclesreverse[4]);
                }
                else  if(nametwo.toLowerCase().contains("6") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentaclesreverse[5]);
                }
                else  if(nametwo.toLowerCase().contains("7") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentaclesreverse[6]);
                }
                else  if(nametwo.toLowerCase().contains("8") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentaclesreverse[7]);
                }
                else  if(nametwo.toLowerCase().contains("9") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentaclesreverse[8]);
                }
                else  if(nametwo.toLowerCase().contains("10") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentaclesreverse[9]);
                }
                else  if(nametwo.toLowerCase().contains("page") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentaclesreverse[10]);
                }
                else  if(nametwo.toLowerCase().contains("knight") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentaclesreverse[11]);
                }
                else  if(nametwo.toLowerCase().contains("queen") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentaclesreverse[12]);
                }

                else  if(nametwo.toLowerCase().contains("king") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentaclesreverse[13]);
                }else  if(nametwo.toLowerCase().contains("ace") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swordsreverse[0]);
                }else  if(nametwo.toLowerCase().contains("2") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swordsreverse[1]);
                }else  if(nametwo.toLowerCase().contains("3") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swordsreverse[2]);
                }else  if(nametwo.toLowerCase().contains("4") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swordsreverse[3]);
                }else  if(nametwo.toLowerCase().contains("5") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swordsreverse[4]);
                }else  if(nametwo.toLowerCase().contains("6") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swordsreverse[5]);
                }else  if(nametwo.toLowerCase().contains("7") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swordsreverse[6]);
                }
                else  if(nametwo.toLowerCase().contains("8") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swordsreverse[7]);
                }else  if(nametwo.toLowerCase().contains("9") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swordsreverse[8]);
                }else  if(nametwo.toLowerCase().contains("10") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swordsreverse[9]);
                }else  if(nametwo.toLowerCase().contains("page") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swordsreverse[10]);
                }
                else  if(nametwo.toLowerCase().contains("knight") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swordsreverse[11]);
                }else  if(nametwo.toLowerCase().contains("queen") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swordsreverse[12]);
                }
                else  if(nametwo.toLowerCase().contains("king") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swordsreverse[13]);
                }


            }else{
                if(nametwo.toLowerCase().contains("ace") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cups[0]);
                }else  if(nametwo.toLowerCase().contains("2") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cups[1]);
                }else  if(nametwo.toLowerCase().contains("3") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cups[2]);
                }else  if(nametwo.toLowerCase().contains("4") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cups[3]);
                }else  if(nametwo.toLowerCase().contains("5") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cups[4]);
                }else  if(nametwo.toLowerCase().contains("6") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cups[5]);
                }else  if(nametwo.toLowerCase().contains("7") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cups[6]);
                }else  if(nametwo.toLowerCase().contains("8") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cups[7]);
                }else  if(nametwo.toLowerCase().contains("9") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cups[8]);
                }else  if(nametwo.toLowerCase().contains("10") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cups[9]);
                }else  if(nametwo.toLowerCase().contains("page") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cups[10]);
                }else  if(nametwo.toLowerCase().contains("knight") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cups[11]);
                }else  if(nametwo.toLowerCase().contains("queen") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cups[12]);
                }else  if(nametwo.toLowerCase().contains("king") && nametwo.toLowerCase().contains("cups") ){
                    image.setImageResource(cups[13]);
                }else  if(nametwo.toLowerCase().contains("ace") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wands[0]);
                }else  if(nametwo.toLowerCase().contains("2") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wands[1]);
                }else  if(nametwo.toLowerCase().contains("3") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wands[2]);
                }else  if(nametwo.toLowerCase().contains("4") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wands[3]);
                }else  if(nametwo.toLowerCase().contains("5") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wands[4]);
                }else  if(nametwo.toLowerCase().contains("6") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wands[5]);
                }else  if(nametwo.toLowerCase().contains("7") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wands[6]);
                }else  if(nametwo.toLowerCase().contains("8") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wands[7]);
                }else  if(nametwo.toLowerCase().contains("9") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wands[8]);
                }else  if(nametwo.toLowerCase().contains("10") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wands[9]);
                }else  if(nametwo.toLowerCase().contains("page") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wands[10]);
                }else  if(nametwo.toLowerCase().contains("knight") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wands[11]);
                }else  if(nametwo.toLowerCase().contains("queen") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wands[12]);
                }
                else  if(nametwo.toLowerCase().contains("king") && nametwo.toLowerCase().contains("wands") ){
                    image.setImageResource(wands[13]);
                }
                else  if(nametwo.toLowerCase().contains("ace") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentacles[0]);
                }
                else  if(nametwo.toLowerCase().contains("2") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentacles[1]);
                }
                else  if(nametwo.toLowerCase().contains("3") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentacles[2]);
                }
                else  if(nametwo.toLowerCase().contains("4") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentacles[3]);
                }
                else  if(nametwo.toLowerCase().contains("5") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentacles[4]);
                }
                else  if(nametwo.toLowerCase().contains("6") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentacles[5]);
                }
                else  if(nametwo.toLowerCase().contains("7") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentacles[6]);
                }
                else  if(nametwo.toLowerCase().contains("8") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentacles[7]);
                }
                else  if(nametwo.toLowerCase().contains("9") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentacles[8]);
                }
                else  if(nametwo.toLowerCase().contains("10") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentacles[9]);
                }
                else  if(nametwo.toLowerCase().contains("page") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentacles[10]);
                }
                else  if(nametwo.toLowerCase().contains("knight") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentacles[11]);
                }
                else  if(nametwo.toLowerCase().contains("queen") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentacles[12]);
                }

                else  if(nametwo.toLowerCase().contains("king") && nametwo.toLowerCase().contains("pentacles") ){
                    image.setImageResource(pentacles[13]);
                }else  if(nametwo.toLowerCase().contains("ace") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swords[0]);
                }else  if(nametwo.toLowerCase().contains("2") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swords[1]);
                }else  if(nametwo.toLowerCase().contains("3") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swords[2]);
                }else  if(nametwo.toLowerCase().contains("4") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swords[3]);
                }else  if(nametwo.toLowerCase().contains("5") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swords[4]);
                }else  if(nametwo.toLowerCase().contains("6") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swords[5]);
                }else  if(nametwo.toLowerCase().contains("7") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swords[6]);
                }
                else  if(nametwo.toLowerCase().contains("8") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swords[7]);
                }else  if(nametwo.toLowerCase().contains("9") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swords[8]);
                }else  if(nametwo.toLowerCase().contains("10") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swords[9]);
                }else  if(nametwo.toLowerCase().contains("page") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swords[10]);
                }
                else  if(nametwo.toLowerCase().contains("knight") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swords[11]);
                }else  if(nametwo.toLowerCase().contains("queen") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swords[12]);
                }
                else  if(nametwo.toLowerCase().contains("king") && nametwo.toLowerCase().contains("swords") ){
                    image.setImageResource(swords[13]);
                }
            }

        }


        title.setText(nametwo);
        general.setText(Html.fromHtml("<font color='#EE0000'>GENERAL: </font>\""+data.get(2)));
        work.setText(Html.fromHtml("<font color='#61cc17'>WORK: </font>\""+data.get(3)));
        love.setText(Html.fromHtml("<font color='#17ccbc'>LOVE: </font>\""+data.get(4)));
        finances.setText(Html.fromHtml("<font color='#be0fff'>FINANCE: </font>\""+data.get(5)));
        health.setText(Html.fromHtml("<font color='#0fd7ff'>HEALTH: </font>\""+data.get(6)));
        spirit.setText(Html.fromHtml("<font color='#ff0fd7'>SPIRITUAL: </font>\""+data.get(7)));
        intro.setText(Html.fromHtml("<font color='#0fbeff'>INTRODUCTION: </font>\""+data.get(8)));


    }
    public void Initialization(){
        image = findViewById(R.id.image);
        intro = findViewById(R.id.intro);
        general = findViewById(R.id.general);
        work = findViewById(R.id.work);
        love = findViewById(R.id.love);
        finances = findViewById(R.id.finance);
        health = findViewById(R.id.health);
        spirit = findViewById(R.id.spirit);
        title = findViewById(R.id.titlename);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        data.clear();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        data.clear();
    }
}
