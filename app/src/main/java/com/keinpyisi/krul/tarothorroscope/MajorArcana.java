package com.keinpyisi.krul.tarothorroscope;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Arrays;

public class MajorArcana extends AppCompatActivity implements View.OnClickListener {
private ImageView fool,magician,priestress,empress,emperor,hiero,lovers,chariot,justice,hermit,fortune,strength,hang,death,tem,devil,tower,star,moon,sun,judge,world;
  String name="";
    String selectedItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_major_arcana);
        Initialization();


    }
    public void Initialization(){
        fool = findViewById(R.id.foolbtn);
        magician = findViewById(R.id.magicianbtn);
        priestress = findViewById(R.id.priestressbtn);
        empress = findViewById(R.id.empressbtn);
        emperor = findViewById(R.id.emperorbtn);
        hiero = findViewById(R.id.hierophantbtn);
        lovers = findViewById(R.id.loversbtn);
        chariot = findViewById(R.id.chariotbtn);
        justice = findViewById(R.id.justicebtn);
        hermit = findViewById(R.id.hermitbtn);
        fortune = findViewById(R.id.fortunebtn);
        strength = findViewById(R.id.strengthbtn);
        hang = findViewById(R.id.hangedbtn);
        death = findViewById(R.id.deathbtn);
        tem = findViewById(R.id.tempbtn);
        devil = findViewById(R.id.devilbtn);
        tower = findViewById(R.id.towerbtn);
        star = findViewById(R.id.starbtn);
        moon = findViewById(R.id.moonbtn);
        sun = findViewById(R.id.sunbtn);
        judge = findViewById(R.id.judgebtn);
        world = findViewById(R.id.worldbtn);
        fool.setOnClickListener(this);
        magician.setOnClickListener(this);
        priestress.setOnClickListener(this);
        empress.setOnClickListener(this);
        emperor.setOnClickListener(this);
        hiero.setOnClickListener(this);
        lovers.setOnClickListener(this);
        chariot.setOnClickListener(this);
        justice.setOnClickListener(this);
        hermit.setOnClickListener(this);
        fortune.setOnClickListener(this);
        strength.setOnClickListener(this);
        hang.setOnClickListener(this);
        death.setOnClickListener(this);
        tem.setOnClickListener(this);
        devil.setOnClickListener(this);
        tower.setOnClickListener(this);
        star.setOnClickListener(this);
        moon.setOnClickListener(this);
        sun.setOnClickListener(this);
        judge.setOnClickListener(this);
        world.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
ImageView img =(ImageView)view;
if(R.id.foolbtn == img.getId()){
   name = "The Fool";
   openAlert(name);
}else if (R.id.magicianbtn == img.getId()){
   name = "The Magician";
    openAlert(name);
}else if (R.id.magicianbtn == img.getId()){
    name = "The Magician";
    openAlert(name);
}else if (R.id.priestressbtn == img.getId()){
    name = "The High Priestess";
    openAlert(name);
}else if (R.id.empressbtn == img.getId()){
    name = "The Empress";
    openAlert(name);
}else if (R.id.emperorbtn == img.getId()){
    name = "The Emperor";
    openAlert(name);
}else if (R.id.hierophantbtn == img.getId()){
    name = "The Hierophant";
    openAlert(name);
}else if (R.id.loversbtn == img.getId()){
    name = "The Lovers";
    openAlert(name);
}else if (R.id.chariotbtn == img.getId()){
    name = "The Chariot";
    openAlert(name);
}else if (R.id.justicebtn == img.getId()){
    name = "Justice";
    openAlert(name);
}else if (R.id.hermitbtn == img.getId()){
    name = "Hermit";
    openAlert(name);
}else if (R.id.fortunebtn == img.getId()){
    name = "The Wheel Of Fortune";
    openAlert(name);
}else if (R.id.strengthbtn == img.getId()){
    name = "Strength";
    openAlert(name);
}else if (R.id.hangedbtn == img.getId()){
    name = "The Hanged Man";
    openAlert(name);
}else if (R.id.deathbtn == img.getId()){
    name = "Death";
    openAlert(name);
}else if (R.id.tempbtn == img.getId()){
    name = "Temperance";
    openAlert(name);
}else if(R.id.devilbtn == img.getId()){
    name = "The Devil";
    openAlert(name);
}else if(R.id.towerbtn == img.getId()){
    name = "The Tower";
    openAlert(name);
}
else if (R.id.starbtn == img.getId()){
    name = "The Star";
    openAlert(name);
}else if (R.id.moonbtn == img.getId()){
    name = "The Moon";
    openAlert(name);
}else if (R.id.sunbtn == img.getId()){
    name = "The Sun";
    openAlert(name);
}else if (R.id.judgebtn == img.getId()){
    name = "Judgment";
    openAlert(name);
}else if (R.id.worldbtn == img.getId()){
    name = "The World";
    openAlert(name);
}

    }
    public void openAlert(String name){
       final String Name = name;

        final AlertDialog.Builder builder = new AlertDialog.Builder(MajorArcana.this);

        // Set the alert dialog title
        builder.setTitle("Choose Position");

        // Initializing an array of flowers
        final String[] position = new String[]{
                "Upright",
                "Reversed",
        };

                /*
                    AlertDialog.Builder setSingleChoiceItems (CharSequence[] items,
                                        int checkedItem,
                                        DialogInterface.OnClickListener listener)

                        Set a list of items to be displayed in the dialog as the content, you will
                        be notified of the selected item via the supplied listener. The list will
                        have a check mark displayed to the right of the text for the checked item.
                        Clicking on an item in the list will not dismiss the dialog. Clicking
                        on a button will dismiss the dialog.

                        Parameters
                            items CharSequence: the items to be displayed.
                            checkedItem int: specifies which item is checked. If -1 no items are checked.
                            listener DialogInterface.OnClickListener: notified when an item on
                                        the list is clicked. The dialog will not be dismissed when
                                        an item is clicked. It will only be dismissed if clicked
                                        on a button, if no buttons are supplied it's up to the
                                        user to dismiss the dialog.
                        Returns
                            AlertDialog.Builder This Builder object to allow for
                                                chaining of calls to set methods
                */

        // Set a single choice items list for alert dialog

        builder.setSingleChoiceItems(
                position, // Items list
                -1, // Index of checked item (-1 = no selection)
                new DialogInterface.OnClickListener() // Item click listener
                {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Get the alert dialog selected item's text
                         selectedItem = Arrays.asList(position).get(i);

                        // Display the selected item's text on snack bar

                    }
                });

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Just dismiss the alert dialog after selection
                // Or do something now
                String nametwo;
                if(selectedItem.toLowerCase().contains("reversed")){

                    nametwo = Name +" "+selectedItem;
                    Intent intent = new Intent(getBaseContext(), CardDetail.class);
                    intent.putExtra("EXTRA_SESSION_ID", nametwo);
                    intent.putExtra("FROM","MAJOR");
                    startActivity(intent);

                }else{
                    nametwo = Name;

                    Intent intent = new Intent(getBaseContext(), CardDetail.class);
                    intent.putExtra("EXTRA_SESSION_ID", nametwo);
                    intent.putExtra("FROM","MAJOR");
                    startActivity(intent);

                }


            }
        });

        // Create the alert dialog
        AlertDialog dialog = builder.create();

        // Finally, display the alert dialog
        dialog.show();
    }
}

